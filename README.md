# MockFreshness

The **MockFreshness** repository provides a utility for handling exposed freshness metrics through Prometheus within the **CODECO** project.

## Description

**MockFreshness** integrates seamlessly with Prometheus, allowing you to monitor freshness metrics efficiently. Whether you're working on CODECO or any other project, this utility simplifies the process of dealing with freshness data.

## How to Use

1. **Clone the Repository**:
    - To get started, clone the repository along with its submodules using the following command:
        ```
        git clone https://gitlab.com/jordi.marias/mockfreshness.git --recurse-submodules
        ```

2. **Configuration**:
    - The basic setup is already configured within the `docker-compose.yml` file.
    - Build the Docker images by running:
        ```
        docker-compose build
        ```

3. **Start the Services**:
    - Once the images are built, start the services with:
        ```
        docker-compose up
        ```

4. **Kubernetes Integration**:
    - For users working with Kubernetes, export the utility to your Kubernetes-based approach.
    - Instantiate multiple instances of the client Docker image.
    - Utilize the exposed metrics to enhance communication performance.

Feel free to explore and adapt **MockFreshness** to your specific needs. Happy monitoring! 🚀.
